import React from 'react';

const NextQuestionBtn = function(){
    return (
        <button className='btn btn-primary btn-sm'>Next</button>
          )
}
const PrevQuestionBtn = function(){
    return (
    <button className='btn btn-info btn-sm'>Back</button>
    )
}

export { NextQuestionBtn,PrevQuestionBtn} 